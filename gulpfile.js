const path = require('path');
var gulp = require('gulp');
var webpack = require('webpack-stream');
var browserSync = require('browser-sync').create();

gulp.task('browser-sync', ['webpack', 'templateToDist'] , function() {
	if (browserSync.active) {
		browserSync.reload();
         return;
    }

    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
});

gulp.task('webpack', function() {
  return gulp.src('app/src/main.ts')
      .pipe(webpack(require('./webpack.config.js')))
      .pipe(gulp.dest('dist/'));
});

gulp.task('templateToDist', function(){
  gulp.src(['app/src/index.html'])
    .pipe(gulp.dest('dist/'));
});

gulp.task('default',['webpack', 'templateToDist', 'browser-sync']);