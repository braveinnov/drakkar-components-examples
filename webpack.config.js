const path = require('path');

const drakkarComponents =  {
  entry: './app/src/main.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: [':data-src']
          }
        }
      }
    ]
  },
  resolve: {
    extensions: [ ".tsx", ".ts", ".js" ],
    alias:{
      'vue': 'vue/dist/vue.js'
    }
  },
  output: {
    filename: 'drakkar-components-examples.js',
    path: path.resolve(__dirname, 'dist')
  }
};

module.exports = drakkarComponents;